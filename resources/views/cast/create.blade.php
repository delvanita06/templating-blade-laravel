@extends('adminlte.master')

@section('title')
    Tambah Data
@endsection

@section('content')

<div>
    <h2> Tambah Data </h2>
        <form action="/cast" method="POST">
            @csrf
            <div class="form-group">
                <label for="title">Nama</label>
                <input type="text" class="form-control" name="nama" id="title" placeholder="Masukan Nama">
                @error('nama')
                    <div class="alert alert-danger">
                        {{$message}}
                    </div>
                @enderror
            </div>
            <div class="form-group">
                <label for="body">Umur</label>
                <input type="number" class="form-control" name="umur" id="body" placeholder="Masukan Umur">
                @error('umur')
                    <div class="alert alert-danger">
                        {{$message}}
                    </div>
                @enderror
            </div>
            <div class="form-group">
                <label for="body">Biodata</label>
                <input type="text" class="form-control" name="bio" id="body" placeholder="Biodata">
                @error('bio')
                    <div class="alert alert-danger">
                        {{$message}}
                    </div>
                @enderror
            </div>
            <button type="submit" class="btn btn-primary">Tambah</button>
        </form>
</div>
    
@endsection