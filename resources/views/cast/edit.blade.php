@extends('adminlte.master')

@section('title')
EDIT CAST {{$cast->id}}
@endsection

@section('content')

<div>
        <h2> Edit {{$cast->id}}</h2>
        <form action="/cast/{{$cast->id}}" method="POST">
            @csrf
            @method('put')
            <div class="form-group">
                <label for="title">Nama</label>
                <input type="text" class="form-control" name="nama" id="title" value="{{$cast->nama}}" placeholder="Masukan Nama">
                @error('nama')
                    <div class="alert alert-danger">
                        {{$message}}
                    </div>
                @enderror
            </div>
            <div class="form-group">
                <label for="title">Umur</label>
                <input type="text" class="form-control" name="umur" id="title" value="{{$cast->umur}}" placeholder="Masukan Umur">
                @error('umur')
                    <div class="alert alert-danger">
                        {{$message}}
                    </div>
                @enderror
            </div>
            <div class="form-group">
                <label for="title">Biodata</label>
                <input type="text" class="form-control" name="bio" id="title" value="{{$cast->bio}}" placeholder="Masukan Biodata">
                @error('bio')
                    <div class="alert alert-danger">
                        {{$message}}
                    </div>
                @enderror
            </div>
            <button type="submit" class="btn btn-primary">Edit</button>
        </form>
</div>
@endsection